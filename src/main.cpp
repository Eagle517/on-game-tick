#include <stdio.h>
#include <windows.h>

#include "MinHook.h"
#include "TSFuncs.hpp"

#define MHTEST(header, status) \
    if (status != MH_OK) { \
        BlPrintf("MHError (%s): %s", header, MH_StatusToString(status)); \
        return false; \
    }

typedef void (__stdcall *DemoGame__processTimeEventFn)(ADDR);
DemoGame__processTimeEventFn DemoGame__processTimeEventOriginal = NULL;
DemoGame__processTimeEventFn DemoGame__processTimeEvent;

void __stdcall DemoGame__processTimeEventHook(ADDR timeEvent)
{
	DemoGame__processTimeEventOriginal(timeEvent);

	unsigned int elapsedTime = *(unsigned int *)(timeEvent + 4);
	if (elapsedTime > 1024)
		elapsedTime = 1024;

	static char buf[16];
	snprintf(buf, 16, "%u", elapsedTime);
	tsf_BlCon__executef(2, "onGameTick", buf);
}

bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	MHTEST("MH_Initialize", MH_Initialize());

	ADDR DemoGame__processTimeEventLoc;
	BlScanHex(DemoGame__processTimeEventLoc, "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 44 24 7C");
	DemoGame__processTimeEvent = (DemoGame__processTimeEventFn)DemoGame__processTimeEventLoc;

	MHTEST("DemoGame__processTimeEvent Create",
	    MH_CreateHook(DemoGame__processTimeEvent,
	        &DemoGame__processTimeEventHook,
	        reinterpret_cast<LPVOID *>(&DemoGame__processTimeEventOriginal)));

	MHTEST("DemoGame__processTimeEvent Enable", MH_EnableHook(DemoGame__processTimeEvent));

	BlPrintf("OnGameTick: init'd");
	return true;
}

bool deinit()
{
	MHTEST("DemoGame__processTimeEvent Disable", MH_DisableHook(DemoGame__processTimeEvent));
	BlPrintf("OnGameTick: deinit'd");
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl OnGameTick(){}
