# On Game Tick
A DLL that adds a TorqueScript callback that is called every game tick.

## Usage
`onGameTick(%dt)`

Called for every DemoGame::processTimeEvent call in the engine. `%dt` is the
delta time between game ticks.  It will never be greater than 1024 as [that is
how the engine behaves](https://github.com/hatf0/tge151/blob/9c6ff74f1949b3a2479e66577ae0acdf1b83d240/engine/game/main.cc#L626).
